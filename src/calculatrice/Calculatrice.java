
package calculatrice;


// Class calculatrice
public class Calculatrice {
    
    public static double addition(double a, double b){
        return a + b;
    }
    
    public static double soustraction(double a, double b){
        return a - b;
    }
    
    public static double multiplication(double a, double b){
        return a * b;
    }
    
    public static double division(double dividende, double diviseur){
        
        if (diviseur == 0){
            throw new ArithmeticException("Division par zéro impossible");
        }
        
        return dividende / diviseur;
    }
    
    public static double moyenne(double[] nombres){
        
        if (nombres.length == 0){
            throw new ArithmeticException("Nombres Not Found");
        }
     
        double somme = 0;
        
        for (double nombre : nombres){
            somme += nombre;
        }
               
        return division(somme, nombres.length);
    }
    
}
