/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rama1
 */
public class CalculatriceTest {
    
    public CalculatriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    @Test
    public void testAddition() {
        assertEquals(15, Calculatrice.addition(7, 8), 0.0);  
    }
    
    @Test
    public void testSoustration() {
        
        assertEquals(10, Calculatrice.soustraction(20, 10),0.0);
        assertEquals(-1, Calculatrice.soustraction(5, 6),0.0);        
        assertEquals(-4, Calculatrice.soustraction(-5, -1),0.0);
    }
    
    @Test
    public void testMultiplication() {
        
        assertEquals(10, Calculatrice.multiplication(5, 2),0.0);
        assertEquals(10, Calculatrice.multiplication(-5, -2),0.0);
        assertEquals(-10, Calculatrice.multiplication(-5, 2),0.0);

    }
    
    @Test
    public void testDivision(){
        assertEquals(5, Calculatrice.division(10, 2),0.0);    
    }
    
    @Test(expected = ArithmeticException.class)
    public void testDivionParZero(){
        Calculatrice.division(10, 0);
        
        fail("Success");
    }
    
    @Test
    public void testMoyenne() {
        
        double[] nombres = {12,12,12};
        assertEquals(12,Calculatrice.moyenne(nombres),0.0);
    }
    
    @Test(expected = ArithmeticException.class)
    public void testMoyenneAvecTableauVide(){
        
        double [] nombres = {};
        Calculatrice.moyenne(nombres);
        
        fail("Success");
    }


}
